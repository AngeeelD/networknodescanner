package mx.uag.angeeeld.networkscanner.utils.sharedPreferences

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import mx.uag.angeeeld.networkscanner.data.local.PingCommandEntity

class SharedPreferencesUtils(context: Context) {


  companion object {

    //region CONSTANTS
    const val MY_SHARED_PREFERENCES = "MY_SHARED_PREFERENCES"
    const val PING_SETTINGS_KEY = "PING_SETTINGS_KEY"
    //endregion

    //region singleton creation
    private var utils: SharedPreferencesUtils? = null

    fun getInstance(context: Context): SharedPreferencesUtils {
      if (utils == null) {
        utils = SharedPreferencesUtils(context)
      }
      return utils as SharedPreferencesUtils
    }
    //endregion
  }

  //regin Global Variables
  private var sharedPreferences: SharedPreferences? = null
  //endregion

  //region init
  init {
    sharedPreferences = context.getSharedPreferences(MY_SHARED_PREFERENCES, Context.MODE_PRIVATE)
  }
  //endregion

  //region Local Methods
  fun setPingSettings(pingCommandEntity: PingCommandEntity) {
    sharedPreferences?.edit()
        ?.putString(SharedPreferencesUtils.PING_SETTINGS_KEY, Gson().toJson(pingCommandEntity))
        ?.apply()
  }

  fun getPingSettings() = Gson().fromJson(sharedPreferences?.getString(PING_SETTINGS_KEY, Gson().toJson(PingCommandEntity())), PingCommandEntity::class.java)!!
  //endregion

}
