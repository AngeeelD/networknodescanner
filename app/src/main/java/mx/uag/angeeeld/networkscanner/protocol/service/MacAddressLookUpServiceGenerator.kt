package mx.uag.angeeeld.networkscanner.protocol.service

import io.reactivex.schedulers.Schedulers
import mx.uag.angeeeld.networkscanner.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

class MacAddressLookUpServiceGenerator {

  companion object {

    private const val API_BASE_URL = "https://macvendors.co/"

    private var builder = Retrofit.Builder()
        .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(API_BASE_URL)

    private val httpClient = OkHttpClient.Builder()
        .writeTimeout(60, TimeUnit.SECONDS)
        .readTimeout(60, TimeUnit.SECONDS)
        .connectTimeout(60, TimeUnit.SECONDS)

    private val logging = HttpLoggingInterceptor()
        .setLevel(HttpLoggingInterceptor.Level.BODY)


    private fun changeBaseApiUrl(apiUrl: String) {
      builder = Retrofit.Builder()
          .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
          .addConverterFactory(GsonConverterFactory.create())
          .baseUrl(apiUrl)
    }

    /**
     * Creates and instance of API definitions class like {@link Authentication API}
     *
     * @param serviceClass .class of API definition class
     * @return an instance of .class
     */
    fun <T : Any> createService(serviceClass: Class<T>): T {
      configureToIgnoreCertificate(httpClient)
      if (!httpClient.interceptors().contains(logging) && BuildConfig.DEBUG) {
        httpClient.addInterceptor(logging)
      }
      builder.client(httpClient.build())

      return builder.build().create(serviceClass)
    }


    //Setting testMode configuration. If set as testMode, the connection will skip certification check
    private fun configureToIgnoreCertificate(builder: OkHttpClient.Builder): OkHttpClient.Builder {
      Timber.w("Ignore Ssl Certificate")
      try {

        // Create a trust manager that does not validate certificate chains
        val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {

          override fun getAcceptedIssuers(): Array<X509Certificate> = arrayOf()

          @Throws(CertificateException::class)
          override fun checkClientTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
          }

          @Throws(CertificateException::class)
          override fun checkServerTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
          }
        })

        // Install the all-trusting trust manager
        val sslContext = SSLContext.getInstance("SSL")
        sslContext.init(null, trustAllCerts, java.security.SecureRandom())
        // Create an ssl socket factory with our all-trusting manager
        val sslSocketFactory = sslContext.socketFactory

        builder.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
        builder.hostnameVerifier { _, _ -> true }
      } catch (e: Exception) {
        Timber.w("Ignore Ssl Certificate")
      }

      return builder
    }


  }

}