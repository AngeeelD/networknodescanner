package mx.uag.angeeeld.networkscanner.views.main

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.afollestad.materialdialogs.MaterialDialog
import kotlinx.android.synthetic.main.activity_main.*
import mx.uag.angeeeld.networkscanner.R
import mx.uag.angeeeld.networkscanner.data.local.NodeEntity
import mx.uag.angeeeld.networkscanner.views.detail.NodeDetailActivity
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

class MainActivity : AppCompatActivity() {

  //region Global Variables
  private val presenter = MainPresenter()
  //endregion

  //region Activity Methods
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    setupViews()
    scanNetworksNodes()
  }

  override fun attachBaseContext(newBase: Context) {
    super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
  }
  //endregion

  //region Local Methods
  private fun scanNetworksNodes() {
    presenter.scanNetworksNodes(onStart = {
      main_activity_loading_container.visibility = View.VISIBLE
      main_activity_error_container.visibility = View.GONE
      main_activity_nodes_container.visibility = View.GONE
      main_activity_nodes_swipe_refresh.isRefreshing = true
    }, onSuccess = { it ->
      main_activity_loading_container.visibility = View.GONE
      main_activity_nodes_swipe_refresh.isRefreshing = false
      populateNetworkNodes(it)
    }, onFail = {
      main_activity_loading_container.visibility = View.GONE
      main_activity_nodes_swipe_refresh.isRefreshing = false
      when (it) {
        MainPresenter.ErrorTypes.ON_BUSY_BOX_REQUIRED -> openBusyBox()
        MainPresenter.ErrorTypes.ON_EMPTY_NODES -> showUnknownError()
        MainPresenter.ErrorTypes.ON_UNKNOWN_ERROR -> showUnknownError()
      }
    })
  }

  private fun setupViews() {
    main_activity_error_container.setOnClickListener {
      main_activity_error_container.visibility = View.GONE
    }
    main_activity_nodes_swipe_refresh.setOnRefreshListener {
      scanNetworksNodes()
    }
  }

  private fun openBusyBox() {
    MaterialDialog.Builder(this)
        .title("upps")
        .content("Parece ser que tu Sistema Operativo no tiene ARP instalado, por favor, Use BusyBox.")
        .positiveText("Usar Busibox")
        .negativeText("Salir")
        .onPositive { _, _ ->
          val packageManager = packageManager
          val intent = packageManager.getLaunchIntentForPackage("stericson.busybox")
          if (intent != null) {
            intent.addCategory(Intent.CATEGORY_LAUNCHER)
            startActivity(intent)
          } else {
            try {
              val i = Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=stericson.busybox"))
              i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
              startActivity(i)
            } catch (anfe: android.content.ActivityNotFoundException) {
              startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=stericson.busybox")))
            }
          }
        }
        .onNegative { _, _ ->
          finish()
        }
        .canceledOnTouchOutside(false)
        .cancelable(false)
        .show()
  }

  private fun showUnknownError() {
    main_activity_error_container.visibility = View.VISIBLE
  }

  private fun populateNetworkNodes(nodeEntities: List<NodeEntity>) {
    val mutableNodes = nodeEntities.toMutableList()
    
    main_activity_nodes_container.visibility = View.VISIBLE
    main_activity_nodes_recycler.setHasFixedSize(true)
    main_activity_nodes_recycler.layoutManager = LinearLayoutManager(this)

    main_activity_nodes_recycler.adapter = NodesAdapter(mutableNodes, this) {
      startActivity(Intent(this@MainActivity, NodeDetailActivity::class.java).apply {
        putExtra(NodeDetailActivity.NODE_EXTRA, it)
      })
    }
  }

  //endregion

}
