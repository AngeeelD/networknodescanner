package mx.uag.angeeeld.networkscanner.data.local

data class PingAttemptEntity(val attempt: Int = 0,
                             val packageSize: Int = 0,
                             val fromIp: String = "",
                             val ttl: Int = 0,
                             val time: Double = 0.0,
                             val timeUnity: String = "")