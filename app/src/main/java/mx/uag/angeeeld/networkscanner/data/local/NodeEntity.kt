package mx.uag.angeeeld.networkscanner.data.local

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NodeEntity(var ip: String? = "",
                      var macAddress: String? = "",
                      var destinationNetworkInterface: String? = "",
                      var sourceNetworkInterface: String? = "",
                      var detailedMacAddress: MacAddress? = null) : Parcelable {

  @Parcelize
  data class MacAddress(@SerializedName("company") var company: String? = null,
                        @SerializedName("mac_prefix") var mac_prefix: String? = null,
                        @SerializedName("address") var address: String? = null,
                        @SerializedName("start_hex") var start_hex: String? = null,
                        @SerializedName("end_hex") var end_hex: String? = null,
                        @SerializedName("country") var country: String? = null,
                        @SerializedName("type") var type: String? = null,
                        @SerializedName("error") var error: String? = null) : Parcelable

}