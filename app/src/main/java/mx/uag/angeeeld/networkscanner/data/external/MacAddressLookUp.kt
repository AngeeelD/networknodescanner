package mx.uag.angeeeld.networkscanner.data.external

import com.google.gson.annotations.SerializedName
import mx.uag.angeeeld.networkscanner.data.local.NodeEntity

data class MacAddressLookUp(@SerializedName("result") var result: NodeEntity.MacAddress? = null)

