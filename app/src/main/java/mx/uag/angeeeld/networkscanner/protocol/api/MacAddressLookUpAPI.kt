package mx.uag.angeeeld.networkscanner.protocol.api

import io.reactivex.Observable
import mx.uag.angeeeld.networkscanner.data.external.MacAddressLookUp
import retrofit2.http.GET
import retrofit2.http.Path

interface MacAddressLookUpAPI {

  @GET("/api/{mac_address}/{format}")
  fun getMacAddressInformation(@Path("mac_address") macAddress: String,
                               @Path("format") format: String = "json"): Observable<MacAddressLookUp>

}