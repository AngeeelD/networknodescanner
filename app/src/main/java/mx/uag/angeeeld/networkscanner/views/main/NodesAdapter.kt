package mx.uag.angeeeld.networkscanner.views.main

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.item_network_node.view.*
import mx.uag.angeeeld.networkscanner.R
import mx.uag.angeeeld.networkscanner.data.local.NodeEntity
import mx.uag.angeeeld.networkscanner.protocol.api.MacAddressLookUpAPI
import mx.uag.angeeeld.networkscanner.protocol.service.MacAddressLookUpServiceGenerator
import timber.log.Timber

class NodesAdapter(val items: List<NodeEntity>,
                   val context: Context,
                   val onItemClick: ((NodeEntity) -> Unit)? = null) : RecyclerView.Adapter<NodesAdapter.NodeViewHolder>() {

  override fun onCreateViewHolder(parent: ViewGroup, p1: Int): NodeViewHolder =
      NodeViewHolder(LayoutInflater.from(context).inflate(R.layout.item_network_node, parent, false))

  override fun getItemCount(): Int = items.size

  override fun onBindViewHolder(holder: NodeViewHolder, position: Int) {
    items[position].let { node ->
      holder.ipPrefix.text = "${node.destinationNetworkInterface}:"
      holder.ip.text = node.ip
      holder.macAddress.text = node.macAddress
      holder.vendor.text = node.detailedMacAddress?.company ?: ""
      holder.fromInterface.text = node.sourceNetworkInterface
      if (node.detailedMacAddress == null && node.macAddress != null) {
        MacAddressLookUpServiceGenerator.createService(MacAddressLookUpAPI::class.java)
            .getMacAddressInformation(node.macAddress!!)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
              node.detailedMacAddress = it.result
              notifyDataSetChanged()
            }, {
              Timber.e(it)
            })
      }
    }
    holder.cardItem.setOnClickListener {
      onItemClick?.invoke(items[position])
    }
  }

  class NodeViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val cardItem = view.network_node_item!!
    val ipPrefix = view.network_node_item_ip_prefix!!
    val ip = view.network_node_item_ip!!
    val macAddress = view.network_node_item_mac_address!!
    val vendor = view.network_node_item_mac_vendor!!
    val fromInterface = view.network_node_item_from_interface!!
  }

}