package mx.uag.angeeeld.networkscanner.views.main

import com.jaredrummler.android.shell.Shell
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import mx.uag.angeeeld.networkscanner.data.local.NodeEntity
import timber.log.Timber
import java.util.concurrent.TimeUnit

class MainPresenter {

  enum class ErrorTypes {
    ON_EMPTY_NODES,
    ON_BUSY_BOX_REQUIRED,
    ON_UNKNOWN_ERROR
  }

  fun scanNetworksNodes(onStart: (() -> Unit)?,
                        onSuccess: ((listOfNodeEntities: List<NodeEntity>) -> Unit)?,
                        onFail: ((error: MainPresenter.ErrorTypes) -> Unit)?) {
    onStart?.invoke()
    val result = Shell.SU.run("arp -a")
    Observable.just(true)
        .delay(5, TimeUnit.SECONDS)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe({
          if (result.isSuccessful) {
            Timber.e(result.getStdout())

            val listOfNodes = parseArpResponse(result.getStdout())
            if (listOfNodes.isNotEmpty()) {
              onSuccess?.invoke(listOfNodes)
            } else {
              onFail?.invoke(ErrorTypes.ON_EMPTY_NODES)
            }
          } else {
            Timber.e(result.getStderr())
            if (result.getStderr().contains("arp: not found")) {
              onFail?.invoke(ErrorTypes.ON_BUSY_BOX_REQUIRED)
            } else {
              onFail?.invoke(ErrorTypes.ON_UNKNOWN_ERROR)
            }
          }
        }, {
          Timber.e(it)
        })
  }

  //FORMAT ? (192.168.2.1) at 4e:32:75:e9:59:64 [ether]  on wlan0
  private fun parseArpResponse(arpOutput: String) =
      arpOutput.split("\n")
          .map {
            val node = NodeEntity()
            val splitValue = it.split(" ")
            splitValue.forEach {
              when {
                it.contains("(") -> node.ip = it.replace("(", "").replace(")", "")
                it.contains(":") -> node.macAddress = it
                it.contains("[") -> node.destinationNetworkInterface = it.replace("[", "").replace("]", "")
                it != "?" && it != "on" -> node.sourceNetworkInterface = it
              }
            }
            node
          }
          .toList()


}