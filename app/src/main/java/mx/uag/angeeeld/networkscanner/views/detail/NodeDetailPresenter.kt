package mx.uag.angeeeld.networkscanner.views.detail

import com.jaredrummler.android.shell.Shell
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import mx.uag.angeeeld.networkscanner.data.local.PingAttemptEntity
import mx.uag.angeeeld.networkscanner.data.local.PingCommandEntity
import timber.log.Timber

class NodeDetailPresenter {

  //region Global Variable
  private val pendingTasks = mutableListOf<Disposable>()
  //endregion

  //region Presenter Methods
  fun checkLatency(pingCommandEntity: PingCommandEntity = PingCommandEntity(),
                   onStart: (() -> Unit)?,
                   onSuccess: ((List<PingAttemptEntity>) -> Unit)?,
                   onFail: (() -> Unit)?) {
    if (pingCommandEntity.host == null) onFail?.invoke()

    onStart?.invoke()
    pendingTasks.add(Observable.just(pingCommandEntity)
        .map { pingCommand(pingCommandEntity) }
        .subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe({
          Timber.e("PING REQUEST FINISHED")
          if (it.isNotEmpty()) {
            onSuccess?.invoke(it)
          } else {
            onFail?.invoke()
          }
        }, {
          Timber.e(it)
          onFail?.invoke()
        }))
  }

  private fun pingCommand(pingCommandEntity: PingCommandEntity): List<PingAttemptEntity> {
    var command = ""
    pingCommandEntity.let {
      command = "ping " +
          "${if (it.count > 0) "-c ${it.count}" else ""} " +
          "${if (it.size > 0) "-s ${it.size}" else ""} " +
          "${if (it.ttl > 0) "-t ${it.ttl}" else ""} " +
          "${if (it.instantResponse) "-A" else ""} " +
          it.host
    }

    val result = Shell.SU.run(command)

    return if (result.isSuccessful) {
      Timber.e(result.getStdout())
      parsePingResult(result.getStdout())
    } else {
      Timber.e(result.getStderr())
      listOf()
    }
  }

  private fun parsePingResult(output: String) =
      try {
        output.split("\n")
            .filter { it.contains("from") }
            .map { it.split(" ") }
            .map {
              PingAttemptEntity(
                  attempt = it[4].split("=")[1].toInt(),
                  packageSize = it[0].toInt(),
                  fromIp = it[3],
                  ttl = it[5].split("=")[1].toInt(),
                  time = it[6].split("=")[1].toDouble(),
                  timeUnity = it[7])
            }
            .toList()
      } catch (e: Exception) {
        listOf<PingAttemptEntity>()
      }

  fun stopPendingTasks() {
    pendingTasks.filter { !it.isDisposed }.forEach { it.dispose() }
  }
  //endregion

}