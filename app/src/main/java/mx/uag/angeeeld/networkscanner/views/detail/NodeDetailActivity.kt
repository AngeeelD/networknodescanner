package mx.uag.angeeeld.networkscanner.views.detail

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import com.afollestad.materialdialogs.MaterialDialog
import kotlinx.android.synthetic.main.activity_node_detail.*
import kotlinx.android.synthetic.main.dialog_ping_settings.view.*
import mx.uag.angeeeld.networkscanner.R
import mx.uag.angeeeld.networkscanner.data.local.NodeEntity
import mx.uag.angeeeld.networkscanner.data.local.PingAttemptEntity
import mx.uag.angeeeld.networkscanner.utils.format
import mx.uag.angeeeld.networkscanner.utils.sharedPreferences.SharedPreferencesUtils
import timber.log.Timber
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

class NodeDetailActivity : AppCompatActivity() {

  //region CONSTANTS
  companion object {
    const val NODE_EXTRA = "NODE_EXTRA"
  }
  //endregion

  //region Global Variables
  private var presenter = NodeDetailPresenter()

  private var nodeEntity: NodeEntity? = null
  //endregion

  //region Activity Methods
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_node_detail)
    setSupportActionBar(toolbar)

    readInitialData()
    setupViews()
  }

  override fun onResume() {
    super.onResume()
    checkLatency()
  }

  override fun onOptionsItemSelected(item: MenuItem?): Boolean {
    return item?.itemId.let {
      when (it) {
        android.R.id.home -> {
          onBackPressed(); true
        }
        else -> super.onOptionsItemSelected(item)
      }
    }
  }

  override fun attachBaseContext(newBase: Context) {
    super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
  }
  //endregion

  //region Local Methods

  private fun checkLatency() {
    presenter.checkLatency(buildPingCommandEntity(), onStart = {
      node_detail_measurement_animation_container.visibility = View.VISIBLE
      node_detail_ping_results_container.visibility = View.GONE
      ping_information_error_container.visibility = View.GONE
      ping_information_result_title.text = "Calculando Latencia"
    }, onSuccess = {
      node_detail_ping_results_container.visibility = View.VISIBLE
      node_detail_measurement_animation_container.visibility = View.GONE
      ping_information_error_container.visibility = View.GONE
      ping_information_result_title.text = "Resultados del cálculo"
      showResults(it)
    }, onFail = {
      ping_information_result_title.text = "Error en el cálculo"
      ping_information_error_container.visibility = View.VISIBLE
      node_detail_measurement_animation_container.visibility = View.GONE
      node_detail_ping_results_container.visibility = View.GONE
    })
  }

  private fun readInitialData() {
    intent.extras?.let {
      nodeEntity = it.getParcelable(NODE_EXTRA) ?: NodeEntity()
    }
    Timber.e("NODE: $nodeEntity")
  }

  private fun setupViews() {
    supportActionBar?.title = "Detalles de ${nodeEntity?.ip} "
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    supportActionBar?.setDisplayShowHomeEnabled(true)
    ping_information_error_container.setOnClickListener { checkLatency() }

    ping_information_ip_value.text = nodeEntity?.ip
    ping_information_mac_value.text = nodeEntity?.macAddress
    nodeEntity?.detailedMacAddress?.company?.let {
      ping_information_hardware_vendor_title.visibility = View.VISIBLE
      ping_information_hardware_vendor_value.visibility = View.VISIBLE
      ping_information_hardware_vendor_value.text = it
    }

    nodeEntity?.detailedMacAddress?.address?.let {
      ping_information_hardware_vendor_address_title.visibility = View.VISIBLE
      ping_information_hardware_vendor_address_value.visibility = View.VISIBLE
      ping_information_hardware_vendor_address_value.text = "$it ${nodeEntity?.detailedMacAddress?.country}"
    }

    ping_information_settings_icon.setOnClickListener {
      val pingSettingsDialog = MaterialDialog.Builder(this)
          .customView(R.layout.dialog_ping_settings, true)
          .title("Ajustes de Ping")
          .positiveText("Guardar")
          .negativeText("Cancelar")
          .onPositive { dialog, _ ->
            SharedPreferencesUtils.getInstance(this)?.let {
              it.setPingSettings(
                  it.getPingSettings()
                      .apply {
                        count = dialog.customView?.ping_settings_package_number_seek_bar?.progress!!
                        size = dialog.customView?.ping_settings_package_size_seek_bar?.progress!!
                        ttl = dialog.customView?.ping_settings_ttl_size_seek_bar?.progress!!
                        instantResponse = dialog.customView?.ping_settings_instant_result_option_check_box?.isChecked!!
                      }
              )
            }
            checkLatency()
          }
          .cancelable(false)
          .canceledOnTouchOutside(false)
          .autoDismiss(true)
          .show()

      pingSettingsDialog.customView?.let { view ->
        SharedPreferencesUtils.getInstance(this).getPingSettings().let {
          view.ping_settings_package_number_seek_bar?.progress = it.count
          view.ping_settings_package_size_seek_bar?.progress = it.size
          view.ping_settings_ttl_size_seek_bar?.progress = it.ttl
          view.ping_settings_instant_result_option_check_box?.isChecked = it.instantResponse
        }
      }
    }
  }

  private fun buildPingCommandEntity() = SharedPreferencesUtils.getInstance(this).getPingSettings().apply { host = nodeEntity?.ip }

  private fun showResults(pingResults: List<PingAttemptEntity>) {
    val minValue = pingResults.map { it.time }.min()!!
    val maxValue = pingResults.map { it.time }.max()!!
    val avgValue = pingResults.map { it.time }.average()
    val oneHundredValue = minValue + maxValue
    val attemptsSuccessfully = pingResults.size
    val maxAttempts = buildPingCommandEntity().count

    ping_information_min_time_progress_value.progress = minValue.times(100).div(oneHundredValue).toInt()
    ping_information_max_time_progress_value.progress = maxValue.times(100).div(oneHundredValue).toInt()
    ping_information_avg_time_progress_value.progress = avgValue.times(100).div(oneHundredValue).toInt()
    ping_information_success_ratio_progress_value.progress = attemptsSuccessfully.times(100).div(maxAttempts)
    ping_information_min_time_value.text = "${minValue.format(2)} ${pingResults[0].timeUnity}"
    ping_information_max_time_value.text = "${maxValue.format(2)} ${pingResults[0].timeUnity}"
    ping_information_avg_time_value.text = "${avgValue.format(2)} ${pingResults[0].timeUnity}"
    ping_information_success_ratio_value.text = "$attemptsSuccessfully/$maxAttempts"
    ping_information_packets.text = "Paquetes: $maxAttempts"
    ping_information_packet_size.text = "Tamaño de paquete: ${pingResults[0].packageSize}"
    ping_information_ttl.text = "TTL: ${pingResults[0].ttl}"
  }
  //endregion

}
