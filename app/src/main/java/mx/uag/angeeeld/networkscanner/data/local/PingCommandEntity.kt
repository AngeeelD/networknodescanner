package mx.uag.angeeeld.networkscanner.data.local

import com.google.gson.annotations.SerializedName

data class PingCommandEntity(@SerializedName("host") var host: String? = null,
                             @SerializedName("count") var count: Int = 10,
                             @SerializedName("size") var size: Int = 56,
                             @SerializedName("ttl") var ttl: Int = 64,
                             @SerializedName("instantResponse") var instantResponse: Boolean = false
)