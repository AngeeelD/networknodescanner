package mx.uag.angeeeld.networkscanner.views.splash

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import mx.uag.angeeeld.networkscanner.R
import mx.uag.angeeeld.networkscanner.views.main.MainActivity
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper


class SplashActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_splash)

    Handler().postDelayed({
      startActivity(Intent(this@SplashActivity, MainActivity::class.java))
      finish()
    }, 3000)
  }

  override fun attachBaseContext(newBase: Context) {
    super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
  }

}
