package mx.uag.angeeeld.networkscanner.application

import android.app.Application
import mx.uag.angeeeld.networkscanner.R
import timber.log.Timber
import uk.co.chrisjenx.calligraphy.CalligraphyConfig

class App : Application() {

  override fun onCreate() {
    super.onCreate()

    Timber.plant(Timber.DebugTree())

    CalligraphyConfig.initDefault(
        CalligraphyConfig.Builder()
            .setDefaultFontPath("fonts/Metropolis-Regular.otf")
            .setFontAttrId(R.attr.fontPath)
            .build())
  }

}